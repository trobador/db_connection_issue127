defmodule Test.Repo.Migrations.Mymigration do
  use Ecto.Migration

  def change do
    create table(:myschema) do
      add :myfield, :string

      timestamps()
    end

  end
end
