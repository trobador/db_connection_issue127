use Mix.Config

# In this file, we keep production configuration that
# you'll likely want to automate and keep away from
# your version control system.
#
# You should document the content of this
# file or create a script for recreating it, since it's
# kept out of version control and might be hard to recover
# or recreate for your teammates (or yourself later on).
config :test, TestWeb.Endpoint,
  secret_key_base: "k8NyI2r95lPgGdmsvRuWyqI4TnGhsCFqKm0DEM5/w4Q5qMYwOwvIBr0wWOEcJMyD"

# Configure your database
config :test, Test.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  #password: "postgres",
  database: "test_prod",
  pool_size: 15
