defmodule TestWeb.PageController do
  use TestWeb, :controller

  alias Test.Repo
  alias Test.MySchema

  def index(conn, _params) do
    render conn, "index.html", entries: Test.Entries.list_entries
  end

  def create(conn, %{"entry" => entry}) do
    Repo.insert(%MySchema{myfield: entry})
    text conn, "New entry: #{entry}"
  end
end
